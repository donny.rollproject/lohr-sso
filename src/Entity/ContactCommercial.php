<?php

namespace Narazima\LohrSso\Entity;

final class ContactCommercial extends AbstractEntity
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $mobile;

    /**
     * @var string
     */
    public $other;

    /**
     * @var string
     */
    public $fax;

    /**
     * @var string
     */
    public $image;

    /**
     * @var string
     */
    public $image_url;

    /**
     * @var int
     */
    public $active;

}
