<?php

namespace Narazima\LohrSso\Entity;

final class Division extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /** 
     * Employees[]
     */
    public $employees;

    /**
     * @Override
     */
    public function build(array $parameters)
    {
        foreach ($parameters as $property => $value) {

            if (property_exists($this, $property)) {
                $values = [];
                if ( $property == 'employees' ) {
                    if ( is_array( $value ) ) {
                        $tmpValue = [];
                        foreach( $value as $employee ) {
                            $tmpValue[] = new Employee($employee);
                        }
                        $value = $tmpValue;
                    }
                }
                $this->$property = $value;
            }
        }
    }

}
