<?php

namespace Narazima\LohrSso\Entity;

final class Language extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $code;

}
