<?php

namespace Narazima\LohrSso\Entity;

final class UserDetail extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @var string
     */
    public $key_name;

    /**
     * @var string
     */
    public $value;

}
