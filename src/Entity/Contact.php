<?php

namespace Narazima\LohrSso\Entity;

final class Contact extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $client_id;

    /**
     * @var int
     */
    public $contact_id;

    /**
     * @var int
     */
    public $name;

    /**
     * @var int
     */
    public $email;

    /**
     * @var string
     */
    public $user_avatar;

    /**
     * @var int
     */
    public $user_type_id;

    /**
     * @var int
     */
    public $user_level_id;

    /**
     * @var int
     */
    public $user_status_id;


    /**
     * @var int
     */
    public $created_at;

    /**
     * @var int
     */
    public $updated_at;

    /**
     * @var int
     */
    public $client;


    /**
     * @Override
     */
    public function build(array $parameters)
    {
        foreach ($parameters as $property => $value) {

            if (property_exists($this, $property)) {

                if ($property == 'client') {
                    $value =  new Client($value);
                }

                $this->$property = $value;    

            }
        }
    }

}
