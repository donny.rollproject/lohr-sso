<?php

/**
 * This file is part of the Lohr SSO library.
 */

namespace Narazima\LohrSso\Api;

use Narazima\LohrSso\Entity\Division as DivisionEntity;

/**
 * @author Narazima <>
 */
class Division extends AbstractApi
{
    /**
     * @return Collection
     */
    public function getAll( $params = [])
    {
        $params['limit'] = 9999;
        $contacts = $this->adapter->get( sprintf('%s/divisions?%s', $this->endpoint, http_build_query( $params ) ) );
        $contacts = json_decode($contacts);

        return array_map(function ($action) {
            return new DivisionEntity($action);
        }, $contacts->data);
    }


    /**
     * Find an object
     * the result was adapted from laravel pagination
     * 
     * @param array $params
     * @return Collection
     */
    public function find($params = array() )
    {        
        $contacts = $this->adapter->get( sprintf('%s/divisions?%s', $this->endpoint, http_build_query($params) ) );
        $contacts = json_decode($contacts);

        $contacts->data = array_map(function ($contact) {
            return new DivisionEntity($contact);
        }, $contacts->data);

        return $contacts;
    }


    /**
     * @param int $id
     *
     * @return Contact
     */
    public function get($id)
    {
        $contact = $this->adapter->get(sprintf('%s/divisions/%d', $this->endpoint, $id));
        $contact = json_decode($contact);
        return new DivisionEntity($contact);
    }

    
    /**
     * @param int $id
     * @param array $data
     *
     * @return Contact|false
     */
    public function update($id, $data = array())
    {
        $contact = $this->adapter->put( sprintf('%s/divisions/%d', $this->endpoint, $id), $data );   
        $contact = json_decode($contact);
        return new DivisionEntity($contact);
    }


    /**
     * @param array     $data
     * 
     * @return ContactEntity
     */
    public function create($data)
    {
        $contact = $this->adapter->post( sprintf('%s/divisions', $this->endpoint), $data );
        $contact = json_decode($contact);
        return new DivisionEntity($contact);
    }

    /**
     * @param int       $id
     *
     * @return stdClass
     */
    public function delete($id)
    {
        $contact = $this->adapter->delete( sprintf('%s/divisions/%d', $this->endpoint, $id) );
        return json_decode($contact);
    }

}
