<?php

/**
 * This file is part of the Lohr SSO library.
 *
 * @author Narazima
 */

namespace Narazima\LohrSso\Api;

use Narazima\LohrSso\Entity\ContactCommercial as ContactCommercialEntity;

/**
 * @author Narazima <>
 */
class ContactCommercial extends AbstractApi
{

    /**
     * @return Collection
     */
    public function getAll()
    {
        $contacts = $this->adapter->get(sprintf('%s/contact-commercials', $this->endpoint));
        $contacts = json_decode($contacts);

        return array_map(function ($action) {
            return new ContactCommercialEntity($action);
        }, $contacts->data);
    }


    /**
     * Find an object
     * the result was adapted from laravel pagination
     * 
     * @param array $params
     * @return Collection
     */
    public function find($params = array() )
    {        
        $contacts = $this->adapter->get( sprintf('%s/contact-commercials?%s', $this->endpoint, http_build_query($params) ) );
        $contacts = json_decode($contacts);

        $contacts->data = array_map(function ($contact) {
            return new ContactCommercialEntity($contact);
        }, $contacts->data);

        return $contacts;
    }


    /**
     * @param int $id
     *
     * @return ContactCommercialEntity
     */
    public function get($id)
    {
        $contact = $this->adapter->get(sprintf('%s/contact-commercials/%s', $this->endpoint, $id));
        $contact = json_decode($contact);
        return new ContactCommercialEntity($contact);
    }

    
    /**
     * @param int $id
     * @param array $data
     *
     * @return ContactCommercialEntity|false
     */
    public function update($id, $data = array())
    {
        $contact = $this->adapter->patch( sprintf('%s/contact-commercials/%s', $this->endpoint, $id), $data );   
        $contact = json_decode($contact);
        return new ContactCommercialEntity($contact);
    }


    /**
     * @param array     $data
     * 
     * @return ContactCommercialEntity
     */
    public function create($data)
    {
        $contact = $this->adapter->post( sprintf('%s/contact-commercials', $this->endpoint, $id), $data );
        $contact = json_decode($contact);
        return new ContactCommercialEntity($contact);
    }

    /**
     * @param int       $id
     *
     * @return stdClass
     */
    public function delete($id)
    {
        $contact = $this->adapter->delete( sprintf('%s/contact-commercials/%s', $this->endpoint, $id) );
        return json_decode($contact);
    }

}
