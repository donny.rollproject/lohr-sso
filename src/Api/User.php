<?php

/**
 * This file is part of the Lohr SSO library.
 * This class represent individual user that registered on Lohr SSO
 * for list of all user consider to look at Contact Entity
 */

namespace Narazima\LohrSso\Api;

use Narazima\LohrSso\Entity\User as UserEntity;

/**
 * @author Narazima <>
 */
class User extends AbstractApi
{

    public function info()
    {
        $user = $this->adapter->get( sprintf('%s/user', $this->endpoint ) );
        $user = json_decode($user);

        $user = new UserEntity($user);

        return $user;
    }

    public function findByEmail( $email )
    {
        $data = ['email' => $email];
        $user = $this->adapter->post( sprintf('%s/user/find', $this->endpoint ), $data );
        $user = json_decode( $user );
        $user = new UserEntity( $user );
        return $user;   
    }

    /**
     * @param int $id
     * @param array $data
     *
     * @return Contact|false
     */
    public function update($id, $data = array())
    {
        $user = $this->adapter->patch( sprintf('%s/user/%d', $this->endpoint, $id), $data );   
        $user = json_decode($user);
        return new UserEntity($user);
    }


    /** 
     * Create
     * 
     * @param array     $data
     */
    public function create( $data )
    {
        $user = $this->adapter->post( sprintf('%s/user', $this->endpoint), $data );   
        $user = json_decode($user);
        return new UserEntity($user);
    }

    /** 
     * Delete
     *
     * @param id        $id
     */
    public function delete($id)
    {
        $user = $this->adapter->delete( sprintf('%s/user/%d', $this->endpoint, $id) );   
        $user = json_decode($user);
    }

}
