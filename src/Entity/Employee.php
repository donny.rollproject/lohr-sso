<?php

namespace Narazima\LohrSso\Entity;

final class Employee extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $phone_2;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $fax;

    /**
     * @var string
     */
    public $photo;

    /**
     * @var int
     */
    public $division_id;

    /**
     * @var int
     */
    public $position_id;

    /**
     * @var int
     */
    public $number;

    /**
     * @var Language[]
     */
    public $languages;

    /**
     * @var Position
     */
    public $position;

    /**
     * @var Division
     */
    public $division;

     /**
     * @Override
     */
    public function build(array $parameters)
    {
        foreach ($parameters as $property => $value) {

            if (property_exists($this, $property)) {
                if ( $property == 'languages' ) {
                    if ( is_array( $value ) ) {
                    	$tmpValue = [];
                        foreach( $value as $language ) {
                            $tmpValue[] = new Language( $language );
                        }
                        $value = $tmpValue;
                    }
                }

                if ( $property == 'position' ) {
                    $value = new Position( $value );
                }

                if ( $property == 'division' ) {
                    $value = new Division( $value );
                }

                $this->$property = $value;    

            }
        }
    }

}
