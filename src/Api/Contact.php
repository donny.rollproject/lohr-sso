<?php

/**
 * This file is part of the Lohr SSO library.
 */

namespace Narazima\LohrSso\Api;

use Narazima\LohrSso\Entity\Contact as ContactEntity;
use Narazima\LohrSso\Entity\Client as ClientEntity;

/**
 * @author Narazima <>
 */
class Contact extends AbstractApi
{
    /**
     * @return Collection
     */
    public function getAll()
    {
        $contacts = $this->adapter->get(sprintf('%s/contacts', $this->endpoint, 200));
        $contacts = json_decode($contacts);

        return array_map(function ($action) {
            return new ContactEntity($action);
        }, $contacts->data);
    }


    /**
     * Find an object
     * the result was adapted from laravel pagination
     * 
     * @param array $params
     * @return Collection
     */
    public function find($params = array() )
    {        
        $contacts = $this->adapter->get( sprintf('%s/contacts?%s', $this->endpoint, http_build_query($params) ) );
        $contacts = json_decode($contacts);

        $contacts->data = array_map(function ($contact) {
            return new ContactEntity($contact);
        }, $contacts->data);

        return $contacts;
    }


    /**
     * @param int $id
     *
     * @return Contact
     */
    public function get($id)
    {
        $contact = $this->adapter->get(sprintf('%s/contacts/%d', $this->endpoint, $id));
        $contact = json_decode($contact);
        return new ContactEntity($contact);
    }

    
    /**
     * @param int $id
     * @param array $data
     *
     * @return Contact|false
     */
    public function update($id, $data = array())
    {
        $contact = $this->adapter->put( sprintf('%s/contacts/%d', $this->endpoint, $id), $data );   
        $contact = json_decode($contact);
        return new ContactEntity($contact);
    }


    /**
     * @param array     $data
     * 
     * @return ContactEntity
     */
    public function create($data)
    {
        $contact = $this->adapter->post( sprintf('%s/contacts', $this->endpoint), $data );
        $contact = json_decode($contact);
        return new ContactEntity($contact);
    }

    /**
     * @param int       $id
     *
     * @return stdClass
     */
    public function delete($id)
    {
        $contact = $this->adapter->delete( sprintf('%s/contacts/%d', $this->endpoint, $id) );
        return json_decode($contact);
    }

}
