<?php
/**
 * Main class for Lohr SSO
 */

namespace Narazima\LohrSso;

use Narazima\LohrSso\Adapter\AdapterInterface;

use Narazima\LohrSso\Api\Contact;
use Narazima\LohrSso\Api\Client;
use Narazima\LohrSso\Api\ContactCommercial;
use Narazima\LohrSso\Api\User;



class LohrSso {

	protected $adapter;

    protected $endpoint;

	public function __construct(AdapterInterface $adapter, $endpoint = null)
    {
        $this->adapter = $adapter;
        $this->endpoint = $endpoint;
    }


    public function contact()
    {
    	return new Contact( $this->adapter, $this->endpoint );
    }

    public function client()
    {
    	return new Client( $this->adapter, $this->endpoint );
    }

    public function contactCommercial()
    {
        return new ContactCommercial( $this->adapter, $this->endpoint );
    }

    public function user()
    {
        return new User( $this->adapter, $this->endpoint );
    }

    public function division()
    {
        return new Division( $this->adapter, $this->endpoint );
    }

}