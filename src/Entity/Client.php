<?php

namespace Narazima\LohrSso\Entity;

final class Client extends AbstractEntity
{
    /**
     * @var string
     */
    public $company_id;

    /**
     * @var string
     */
    public $establishment_id;
    

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $name;

    /**
     * @var int
     */
    public $contact_commercial_id;

    /**
     * @var int
     */
    public $logo;

    /**
     * @var string
     */
    public $language_id;

    /**
     * @var int
     */
    public $logo_url;

     /**
     * @var string
     */
    public $transfered; 

    /**
     * @var string
     */
    public $created_at;

    /**
     * @var int
     */
    public $updated_at;

    /**
     * @var ContactCommercial 
     */
    public $contact_commercial;


    /**
     * @Override
     */
    public function build(array $parameters)
    {
        foreach ($parameters as $property => $value) {

            if (property_exists($this, $property)) {

                if ($property == 'contact_commercial') {
                    $value =  new ContactCommercial($value);
                }

                $this->$property = $value;    

            }
        }
    }

}
