<?php

namespace Narazima\LohrSso\Entity;

final class Service extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $name;

}
