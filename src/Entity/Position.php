<?php

namespace Narazima\LohrSso\Entity;

final class Position extends AbstractEntity
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

}
