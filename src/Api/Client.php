<?php

/**
 * This file is part of the Lohr SSO library.
 */

namespace Narazima\LohrSso\Api;

use Narazima\LohrSso\Entity\Contact as ContactEntity;
use Narazima\LohrSso\Entity\Client as ClientEntity;

/**
 * @author Narazima <>
 */
class Client extends AbstractApi
{

    /**
     * @return Collection
     */
    public function getAll()
    {
        $contacts = $this->adapter->get(sprintf('%s/clients', $this->endpoint));
        $contacts = json_decode($contacts);

        return array_map(function ($action) {
            return new ClientEntity($action);
        }, $contacts->data);
    }

    /**
     * Find an object
     * the result was adapted from laravel pagination
     * 
     * @param array $params
     * @return Collection
     */
    public function find($params = array() )
    {        
        $contacts = $this->adapter->get( sprintf('%s/clients?%s', $this->endpoint, http_build_query($params) ) );
        $contacts = json_decode($contacts);

        $contacts->data = array_map(function ($contact) {
            return new ClientEntity($contact);
        }, $contacts->data);

        return $contacts;
    }

    public function getLanguages(){
        $languages = $this->adapter->get( sprintf('%s/languages', $this->endpoint ) );
        $languages = json_decode( $languages );

        return array_map( function($row){ return $row->language_id; }, $languages->data );
    }
    
    /** 
     * Get Client Types
     *
     * @return array
     */
    public function getClientTypes()
    {
        $clientTypes = $this->adapter->get( sprintf('%s/client-types', $this->endpoint ) );
        return json_decode( $clientTypes );
        
    }


    /**
     * @param int $id
     *
     * @return Contact
     */
    public function get($id)
    {
        $contact = $this->adapter->get(sprintf('%s/clients/%d', $this->endpoint, $id));
        $contact = json_decode($contact);
        return new ClientEntity($contact);
    }

    
    /**
     * @param int $id
     * @param array $data
     *
     * @return Contact|false
     */
    public function update($id, $data = array())
    {
        $contact = $this->adapter->patch( sprintf('%s/clients/%d', $this->endpoint, $id), $data );   
        $contact = json_decode($contact);
        return new ClientEntity($contact);
    }


    public function create($data)
    {
        $contact = $this->adapter->post( sprintf('%s/clients', $this->endpoint), $data );   
        $contact = json_decode($contact);
        return new ClientEntity($contact);
    }

    public function delete($id)
    {
        $contact = $this->adapter->delete( sprintf('%s/clients/%d', $this->endpoint, $id) );   
        $contact = json_decode($contact);
    }

}
